import json
import discord
import asyncio
import subprocess
from cogs import TwitchAlerts
from discord.ext import commands


class Ghetto(commands.Bot):
    #session = aiohttp.ClientSession()

    def __init__(self, *args, **kwargs):
        self.java_compatibility = kwargs.pop("java_compatible")
        self._java_process: subprocess = None

        super().__init__(*args, command_prefix=self.prefix_getter(), **kwargs)

    @staticmethod
    def prefix_getter():
        with open("data/config/setup.json") as l:
            prefixes = json.load(l)['prefix']
        return prefixes

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self._java_process:
            self._java_process.terminate()


def main(bot_class=Ghetto, java_compatible: bool = False):
    bot = bot_class(description="A Discord bot that allows you to do things that you never though would be possible."
                                "\nMade by: SoulSenGaming1321",
                    java_compatible=java_compatible)

    async def startup_background_task():
        await bot.wait_until_ready()
        print("Startup background task started...")
        while not bot.is_closed():
            print("inside here!")
            with open("data/Twitch/streamers.json") as d:
                data = json.load(d)
            await TwitchAlerts.TwitchAlerts(bot).check_for_live(data)
            await asyncio.sleep(10)

    async def get_terminal_output():
        await bot.wait_until_ready()

        #May change this later \/, to capture stuff.
        while not bot.is_closed():
            pipe = subprocess.Popen("pwd", shell=True, stdout=subprocess.PIPE).stdout
            output = pipe.read()

    @bot.event
    async def on_ready():
        bot.load_extension("cogs.CogStarter")
        print('\nGhetto bot ready')
        string = "servers" if len(bot.guilds) > 1 else "server"
        await bot.change_presence(status=discord.Status.online,
                                  activity=discord.Activity(type=discord.ActivityType.watching,
                                                            name=f"{len(bot.guilds)} {string} | {bot.command_prefix[0]}help"))
        #bot.loop.create_task(startup_background_task())

    return bot


async def init(bot):
    with open('data/config/login.json') as l:
        _token = json.load(l)['token']

    if _token:
        await bot.login(_token, bot=True)
    else:
        print("No login credentials")
        raise RuntimeError

    await bot.connect()
