import ghetto
import asyncio
import discord
import platform
import subprocess
from cogs import GhettoErrors


def lavalink_compatibility_check():
    if platform.system().lower() == "mac":
        print("Oops! Music cog could not load because Lavalink is not compatible with Mac")
        return False
    elif platform.system().lower() == "windows" or "linux":
        f = subprocess.check_output(["java", "-version"], stderr=subprocess.STDOUT, shell=True)
        f = f.decode("utf8")
        vers = f.split('"')[1]
        vers = vers.split(".")[0]
        if int(vers) >= 10:
            return True
        else:
            print("Oops! Music cog could not load because Lavalink needs Java to run")
            return False
    else:
        raise GhettoErrors.UndeterminedOS("Could not determine operating system")


if __name__ == "__main__":
    bot = ghetto.main(java_compatible=lavalink_compatibility_check())
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(ghetto.init(bot))
    except discord.LoginFailure:
        print('Invalid Login, or discord dumb')
    except KeyboardInterrupt:
        loop.run_until_complete(bot.logout())
    except Exception:
        loop.run_until_complete(bot.logout())
