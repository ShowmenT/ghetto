import asyncio
import discord
from . import Paginator
from discord.ext import commands


class General:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="ping")
    async def _ping(self, ctx):
        em = discord.Embed(title=f"PONG! Ping: {round(self.bot.latency, 2)} MS", colour=0x206694)
        await ctx.send(embed=em)

    @commands.command(name="test2")
    async def _test2(self, ctx):
        lst_counter = 0
        keep_waiting = True

        em = discord.Embed(title="Help Command")
        em.description = "Use things"
        lst = await self.test(ctx)
        msg = await ctx.send(embed=em)
        await msg.add_reaction("⏭")
        await msg.add_reaction("⏹")
        await asyncio.sleep(0.3)

        def check(reaction, user):
            if str(reaction.emoji) == "⏹":
                keep_waiting = False
            return str(reaction.emoji) == "⏭" or str(reaction.emoji) == "⏹", user == ctx.author

        while keep_waiting:
            reaction, user = await self.bot.wait_for("reaction_add", check=check)
            if str(reaction.emoji) == "⏹":
                await msg.edit(embed=discord.Embed(title="Stopped"))
                return
            if lst_counter == len(lst):
                lst_counter = 0
            else:
                f = lst[lst_counter]
                await msg.edit(embed=f)
                lst_counter += 1
                return
            f = lst[lst_counter]
            await msg.edit(embed=f)

    async def test(self, ctx):
        lst = []
        for cog in self.bot.cogs:
            page = Paginator.GhettoPaginator(self.bot)

            cog_name = str(cog)
            cog_commands = self.bot.get_cog_commands(cog)

            page.embed.title = f"{cog_name}:"

            if cog_commands == set():
                continue

            for command in cog_commands:
                command_name = str(command)

                g = self.bot.get_command(str(command))
                aliases_str = ""
                aliases = g.aliases if g.aliases else "None"

                if isinstance(aliases, list):
                    for g in aliases:
                        if len(aliases) == 1:
                            break
                        aliases_str = aliases_str + f"{g}, "
                    aliases_str = aliases_str + f"{g}"
                elif aliases == "None":
                    aliases_str = aliases

                page.add_line(name=f"{ctx.prefix}{command_name}", value=f"Aliases: {aliases_str}")

            lst.append(page.pages)

        return lst


def setup(bot):
    bot.add_cog(General(bot))
