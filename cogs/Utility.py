import discord
from discord.ext import commands


class Utility:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="currency")
    async def _currency(self, ctx, amount: float, currency: str, *, translated_currency: str):
        await ctx.send()

    @commands.command(name="google")
    async def _google_search(self, ctx, query: str):
        em = discord.Embed(title="Google Search Results")
        await ctx.send(embed=em)


def setup(bot):
    bot.add_cog(Utility(bot))
