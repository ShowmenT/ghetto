import random
import discord
from discord.ext import commands


class VideoGames:
    def __init__(self, bot):
        self.bot = bot
        self.queue_list = {}

    async def wait_for_user(self, game, user):
        while not self.bot.is_closed:
            if self.queue_list.get(game.lower()):
                user_found = random.choice(self.queue_list.get(game.lower()))
                if not user_found.lower() == user.lower():
                    #Check's if DM's are enabled or not
                    await user.send(f"Player Found! {user_found}")

    @commands.command(name="gamequeue", aliases=["lfg"])
    async def _game_queue(self, ctx, game: str):
        if self.queue_list.get(game.lower()):
            user = random.choice(self.queue_list.get(game.lower()))
            self.queue_list.pop(user)
            await ctx.send(f"Found {user}")
        else:
            self.queue_list.setdefault(game, []).append(ctx.message.author)
            self.bot.loop.create_task(self.wait_for_user(game.lower(), ctx.message.author))


def setup(bot):
    bot.add_cog(VideoGames(bot))
