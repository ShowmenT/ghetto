class UnvalidTwitchUser(RuntimeError):
    pass


class TooManyRequests(RuntimeWarning):
    pass


class UndeterminedOS(RuntimeError):
    pass
