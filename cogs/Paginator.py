import discord


class GhettoPaginator:
    def __init__(self, bot, max_size=2000):
        self.bot = bot
        self.embed = discord.Embed()
        self._pages = []

    def add_title(self, **kwargs):
        title = kwargs.get("title")
        description = kwargs.get("description")
        self.embed.title = title
        self.embed.description = description

    def add_line(self, name: str='', value: str=''):
        self.embed.add_field(name=name, value=value)

    @property
    def pages(self):
        return self.embed

