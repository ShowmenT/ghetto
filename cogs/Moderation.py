import discord
import typing
import json
from discord.ext import commands


class Moderation:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="ban_formatter")
    @commands.has_permissions(ban_members=True)
    async def _ban_formatter(self, ctx, content: str):
        with open("data/guilds/ban_formats.json") as b:
            data = json.load(b)

        print(content)
        if "```" in content:
            content = content.split("```")
            print(content)
            content = content[1]

            data['guilds'][0][str(ctx.message.guild.id)] = content

        with open("data/guilds/ban_formats.json", "w") as b:
            json.dump(data, b)

        await ctx.send("Formatted to: " + content)

    @commands.command(name="ban")
    @commands.has_permissions(ban_members=True)
    @commands.bot_has_permissions(ban_members=True)
    async def _ban(self, ctx, user: discord.User, reason: str = None, days: typing.Union[int] = 60):
        with open("data/guilds/ban_formats.json") as b:
            data = json.load(b)

        if data['guilds'][0][str(ctx.message.guild.id)]:
            em = discord.Embed(title=f"You have been permanently banned from {ctx.message.guild.name}",
                               description=f"{data['guilds'][0][str(ctx.message.guild.id)]}")
        else:
            em = discord.Embed(title=f"You have been permanently banned from {ctx.message.guild.name}",
                               description=f"Reason: {reason}")
        await ctx.send(embed=em)
        #await self.bot.ban(user, reason=reason, delete_message_days=days)

    @commands.command(name="audit_check")
    @commands.has_permissions(view_audit_log=True)
    @commands.bot_has_permissions(view_audit_log=True)
    async def _audit_check(self, ctx, user: discord.Member, limit: int):
        string = ""
        async for entry in ctx.message.guild.audit_logs(user=user, limit=limit):
            print(entry)
            string = string + f'User: {entry.user} | Action: {entry.action} | Target: {entry.target}\n'
        em = discord.Embed(title=f"Audit Log for: {user}", description=string)
        await ctx.send(embed=em)



def setup(bot):
    bot.add_cog(Moderation(bot))
