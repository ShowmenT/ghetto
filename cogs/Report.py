import discord
from discord.ext import commands


class Report:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="report")
    async def _report(self, ctx, user: discord.Member, reason: str):
        await ctx.send("REPORTED AND DEPORTED!")


def setup(bot):
    bot.add_cog(Report(bot))
