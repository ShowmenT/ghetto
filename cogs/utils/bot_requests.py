import json
import ghetto
import string
import random
import aiohttp
import discord
from . import const
from . import private_const
from .. import GhettoErrors


async def random_string():
    return ''.join(random.choice(string.ascii_uppercase) for _ in range(5))

aiohttpSession = aiohttp.ClientSession()

class NSFW_Requests:
    async def getNSFWJSON(self, session=aiohttpSession):
        with open('data/NSFW/subreddits.json') as r:
            subreddit = random.choice(json.load(r)['subreddit'])

        header = await random_string()

        async with session as ses:
            response = await ses.get(f"https://www.reddit.com/r/{subreddit}/random.json",
                                     headers={'User-agent': header})

        if response.status == 429:
            raise GhettoErrors.TooManyRequests

        return await response.json(), subreddit

    async def getHENTAIJSON(self, session=aiohttpSession):
        with open('data/NSFW/subreddits.json') as r:
            subreddit = random.choice(json.load(r)['hentai_subreddits'])

        header = await random_string()

        async with session as ses:
            response = await ses.get(f"https://www.reddit.com/r/{subreddit}/random.json",
                                     headers={'User-agent': header})

        if response.status == 429:
            raise GhettoErrors.TooManyRequests

        return await response.json(), subreddit

    async def getNEKOS(self, session=aiohttpSession, category=None):
        with open("data/NSFW/nekos.json") as r:
            nekos = json.load(r)["nekos"]
            if category is None:
                nekos = random.choice(nekos)
            else:
                for x in nekos:
                    if category.lower() == x.lower():
                        nekos = x

        header = await random_string()

        async with session as ses:
            response = await ses.get(f"{const.NEKO_BASE_URL}{nekos}",
                                     headers={'User-agent': header})

        if response.status == 429:
            raise GhettoErrors.TooManyRequests

        return await response.json(), nekos


class Twitch_Requests:
    async def getVALIDUSER(self, user: str, session=aiohttpSession):
        header = await random_string()

        async with session as ses:
            response = await ses.get(f"{const.REDDIT_BASE_URL}/users/{user}?client_id={private_const.REDDIT_CLIENT_ID}&token={private_const.REDDIT_TOKEN}",
                                     headers={'User-agent': header})

        if response.status == 429:
            raise GhettoErrors.TooManyRequests

        twitchJSON = await response.json()
        if "error"in twitchJSON:
            return False
        else:
            return True

    async def livestreamSTATUS(self, user: str, session=aiohttpSession):
        header = await random_string()

        async with session as ses:
            response = await ses.get(f"{const.REDDIT_BASE_URL}/streams/{user}?client_id={private_const.REDDIT_CLIENT_ID}&token={private_const.REDDIT_TOKEN}",
                                     headers={'User-agent': header})

        if response.status == 429:
            raise GhettoErrors.TooManyRequests

        json = await response.json()
        if json["stream"]:
            print("returned true")
            return True
        else:
            print("returned false")
            return False

class Anime_Requests:
    async def getANIME(self, anime: str, session=aiohttpSession):
        header = await random_string()
        anime = anime.replace(" ", "%20")

        async with session as ses:
            response = await ses.get(f"https://kitsu.io/api/edge/anime?filter[text]={anime}",
                                     headers={'User-agent': header})

        if not response.status == 200:
            raise GhettoErrors.TooManyRequests("Invalid Status!")

        json = await response.json()



