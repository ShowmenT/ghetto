import json
import asyncio
import discord
from .utils import bot_requests
from discord.ext import commands


class TwitchAlerts:
    def __init__(self, bot):
        self.bot = bot

    async def check_for_live(self, data):
        print("I'm in check for live function!")
        for server_id in data["guilds"][0]:
            for streamer in data["guilds"][0][str(server_id)][0]["streamers"]:
                for channel_id in data["guilds"][0][str(server_id)][0]["notification_channel"]:
                    channel = self.bot.get_channel(int(channel_id))
                    await Alerter(self.bot, channel, streamer).alert()

    @commands.command(name="alertchannel")
    async def _alert_channel(self, ctx, channel: discord.TextChannel):
        with open('data/Twitch/streamers.json') as s:
            data = json.load(s)

        async def data_handler():
            try:
                data["guilds"][0][str(ctx.message.guild.id)][0].setdefault("notification_channel", []).append(str(channel.id))
            except Exception:
                data["guilds"][0][str(ctx.message.guild.id)] = [dict()]
                return await data_handler()

        await data_handler()

        with open('data/Twitch/streamers.json', "w") as s:
            json.dump(data, s)

    @commands.command(name="addstreamer")
    async def _add_streamer(self, ctx, streamer: str):
        if await bot_requests.Twitch_Requests().getVALIDUSER(streamer):
            pass

        with open("data/Twitch/streamers.json") as s:
            data = json.load(s)

        async def data_handler():
            try:
                data["guilds"][0][str(ctx.message.guild.id)][0].setdefault("streamers", []).append(streamer)
            except Exception:
                data["guilds"][0][str(ctx.message.guild.id)] = [dict()]
                return await data_handler()

        await data_handler()

        with open("data/Twitch/streamers.json", "w") as s:
            json.dump(data, s)

        await ctx.send(f"Success! You have added {streamer} to your Twitch Alerts!")


class Alerter:
    def __init__(self, bot, channel, streamer):
        self.bot = bot
        self.channel = channel
        self.streamer = streamer
        self.notified = False

        print("Alerter has been initialized")

        self.bot.loop.create_task(self.unnotify_background_task())

    async def alert(self):
        if self.notified is None:
            return
        print("I'm IN ALERT FUNCTION!")
        if await bot_requests.Twitch_Requests().livestreamSTATUS(self.streamer):
            await self.channel.send(f"Hey! {self.streamer} has started streaming!")
            print("SENT MESSAGE!")
            self.notified = True

    async def unnotify_background_task(self):
        await self.bot.wait_until_ready()
        print("IN UNNOTIFY BACKGROUND TSAK!")
        while not self.bot.is_closed():
            print("Inside not closed hting")
            await asyncio.sleep(10)
            print("after sleep")
            if await bot_requests.Twitch_Requests().livestreamSTATUS(self.streamer):
                if self.notified:
                    print("about to unnotify")
                    self.notified = None
                    print(f"just unnotified {self.streamer}")


def setup(bot):
    bot.add_cog(TwitchAlerts(bot))
