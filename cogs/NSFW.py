import json
import discord
from .utils import bot_requests
from discord.ext import commands


class NSFW:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="randomnsfw")
    @commands.cooldown(3, 10, type=commands.BucketType.user)
    @commands.is_nsfw()
    async def _randnsfw(self, ctx):
        NSFWJSON, subreddit = await bot_requests.NSFW_Requests().getNSFWJSON()
        try:
            img_link = NSFWJSON[0]['data']["children"][0]["data"]["url"]
            title = NSFWJSON[0]['data']["children"][0]["data"]["title"]
        except Exception:
            return self._randnsfw(ctx)
        em = discord.Embed(title=f"Subreddit: {subreddit} | Title: {title}", colour=0x206694)
        if "https://imgur.com/" in img_link:
            img_link = img_link.split("https://imgur.com/")[1]
            img_link = "https://i.imgur.com/" + img_link + ".png"
        elif "https://gfycat.com/" in img_link:
            img_link = img_link.split('.com/')[1]
            img_link = f"https://thumbs.gfycat.com/{img_link}-size_restricted.gif"
        em.set_image(url=img_link)
        em.set_footer(text=f"Requested by: {ctx.message.author}", icon_url=ctx.message.author.avatar_url)
        await ctx.send(embed=em)

    @commands.group(name="hentai", invoke_without_command=False)
    @commands.cooldown(3, 10, type=commands.BucketType.user)
    @commands.is_nsfw()
    async def _hentai(self, ctx):
        pass

    @_hentai.command(name="neko")
    @commands.cooldown(3, 10, type=commands.BucketType.user)
    async def _neko(self, ctx, category: str = None):
        NEKOJSON, title = await bot_requests.NSFW_Requests().getNEKOS(category=category)
        em = discord.Embed(title=f"Nekos.life", colour=0x206694)
        try:
            img_link = NEKOJSON["url"]
        except KeyError:
            with open("data/NSFW/nekos.json") as c:
                data = json.load(c)['nekos']
            em.add_field(name="Categories", value=f"```{', '.join(data)}```")
            em.set_footer(text=f"Requested by: {ctx.message.author}", icon_url=ctx.message.author.avatar_url)
            return await ctx.send(embed=em)
        em.set_image(url=img_link)
        em.set_footer(text=f"Requested by: {ctx.message.author} | {title}", icon_url=ctx.message.author.avatar_url)
        await ctx.send(embed=em)

    @_hentai.command(name="random")
    @commands.cooldown(3, 10, type=commands.BucketType.user)
    async def _random(self, ctx):
        HENTAIJSON, subreddit = await bot_requests.NSFW_Requests().getHENTAIJSON()
        try:
            img_link = HENTAIJSON[0]['data']["children"][0]["data"]["url"]
            title = HENTAIJSON[0]['data']["children"][0]["data"]["title"]
        except Exception:
            return self._random(ctx)
        if "https://imgur.com/" in img_link:
            img_link = img_link.split("https://imgur.com/")[1]
            img_link = "https://i.imgur.com/" + img_link + ".png"
        elif "https://gfycat.com/" in img_link:
            img_link = img_link.split('.com/')[1]
            img_link = f"https://thumbs.gfycat.com/{img_link}-size_restricted.gif"
        em = discord.Embed(title=f"Subreddit: {subreddit} | {title}", colour=0x206694)
        em.set_image(url=img_link)
        await ctx.send(embed=em)


def setup(bot):
    bot.add_cog(NSFW(bot))
