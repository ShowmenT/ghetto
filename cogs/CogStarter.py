import discord
import asyncio
import subprocess
from discord.ext import commands


class CogStarter:
    def __init__(self, bot):
        self.bot = bot
        self._cog_extensions = ['cogs.NSFW',
                                'cogs.Anime',
                                'cogs.Utility',
                                'cogs.General',
                                'cogs.Moderation',
                                'cogs.EventHandler',
                                'cogs.TwitchAlerts',
                                'cogs.ErrorHandler']

        if bot.java_compatibility:
            self._cog_extensions.append("cogs.Music")

        asyncio.get_event_loop().create_task(self.cog_starter())

    async def _async_wait(self, time: float = 1):
        await asyncio.sleep(time)

    async def cog_starter(self):
        for cogs in self._cog_extensions:
            if cogs == "cogs.Music":
                self.bot._java_process = subprocess.Popen(f"java -jar lavalink/Lavalink.jar", shell=True)
                await self._async_wait(12)
            self.bot.load_extension(cogs)
            print(f"Loaded: {cogs}")


def setup(bot):
    bot.add_cog(CogStarter(bot))
