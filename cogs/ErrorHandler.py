import discord
import traceback
from discord.ext import commands


class ErrorHandler:
    def __init__(self, bot):
        self.bot = bot

    async def on_command_error(self, ctx, error):
        if isinstance(error, commands.CommandOnCooldown):
            em = discord.Embed(title="__Error__")
            em.add_field(name="Cooldown", value=f"This command is in cooldown, and has {round(error.retry_after, 2)} left.")
            await ctx.send(embed=em)
        elif isinstance(error, discord.InvalidArgument):
            em = discord.Embed(title="__Error__")
            em.add_field(name="Invalid Argument", value="You have entered a invalid argument.")
            await ctx.send(embed=em)
        else:
            print("".join(traceback.format_exception(type(error), error, error.__traceback__)))
            print(error)


def setup(bot):
    bot.add_cog(ErrorHandler(bot))
